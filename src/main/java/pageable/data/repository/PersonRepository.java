package pageable.data.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import pageable.data.repository.models.Person;

public interface PersonRepository extends PagingAndSortingRepository<Person, Integer> {

   Page<Person> findAllByGender(String gender, Pageable pageable);

}
