package pageable.data.repository.models;

import com.fasterxml.jackson.annotation.JsonInclude;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "mock_data")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Person {

   @Id
   @Column(name = "id")
   private String id;

   @Column(name = "first_name")
   private String firstName;

   @Column(name = "last_name")
   private String lastName;

   @Column(name = "email")
   private String email;

   @Column(name = "gender")
   private String gender;

}
