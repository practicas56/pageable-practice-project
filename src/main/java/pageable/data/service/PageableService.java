package pageable.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import pageable.data.repository.PersonRepository;
import pageable.data.repository.models.Person;

@Service
public class PageableService {

   private final PersonRepository personRepository;

   @Autowired
   public PageableService(PersonRepository personRepository) {
      this.personRepository = personRepository;
   }

   public Page<Person> getAllMale(int page, int size) {
      return personRepository.findAllByGender("Male", PageRequest.of(page, size));
   }

}
