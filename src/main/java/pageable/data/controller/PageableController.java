package pageable.data.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pageable.data.repository.models.Person;
import pageable.data.service.PageableService;

@RestController
@RequestMapping
public class PageableController {

   private final PageableService pageableService;

   @Autowired
   public PageableController(PageableService pageableService) {
      this.pageableService = pageableService;
   }

   @GetMapping("/AllMale")
   public Page<Person> getAllMale(@RequestParam int page, @RequestParam int size) {
      return pageableService.getAllMale(page, size);
   }

}
